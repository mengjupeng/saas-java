package com.yhy.form.service;

import com.yhy.common.service.BaseService;
import com.yhy.form.dao.FormSetFieldDao;
import com.yhy.form.vo.FormSetFieldVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:46 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class FormSetFieldService extends BaseService<FormSetFieldVO> {


    @Autowired
    private FormSetFieldDao baseDao;

    @Override
    protected FormSetFieldDao getDao() {
        return baseDao;
    }

    public List<FormSetFieldVO> findByMainId(String mainId) {
        return getDao().findByMainId(mainId);
    }

    public Integer updateFieldPrivFlag(String id, String privFlag) {
        return getDao().updateFieldPrivFlag(id,privFlag);
    }
}
