package com.yhy.form.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import com.yhy.form.vo.FormSetMainVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午10:54 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formSetMainDao")
public interface FormSetMainDao extends BaseMainDao<FormSetMainVO> {

    int updateFormWidth(@Param("id")String id, @Param("formWidth")Integer formWidth);

}
