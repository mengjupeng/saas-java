package com.yhy.admin.dao;

import com.yhy.common.dao.BaseMainDao;
import com.yhy.common.vo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:08 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysUserDao")
public interface SysUserDao extends BaseMainDao<SysUser> {

    void updateAvatar(@Param("userAccount") String userAccount, @Param("avatar") String avatar);

    void updatePassword(@Param("userAccount") String userAccount, @Param("password") String password);

    void updateCpyOrgSet(@Param("userAccount")String userAccount,@Param("cpyCode")String cpyCode,@Param("orgCode")String orgCode);

    void updateOrgSet(@Param("userAccount")String userAccount, @Param("orgCode")String orgCode);


}
