package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:37 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class SysJobVO extends BaseMainEntity {
	
    /**
     * 组织名称 db_column: JOB_NAME 
     */	
	private String jobName;
    /**
     * 类别(公司，部门，员工) db_column: JOB_TYPE 
     */	
	private String jobType;
    /**
     * 组织编号（系统ID+自动编码） db_column: JOB_CODE 
     */	
	private String jobCode;
    /**
     * 组织状态 db_column: JOB_STATUS 
     */	
	private String jobStatus;
    /**
     * cpyCode db_column: CPY_CODE 
     */	
	private String cpyCode;
    /**
     * 第三方编号 db_column: THIRD_CODE 
     */	
	private String thirdCode;

	public SysJobVO(){
	}

	public SysJobVO( String id ){
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobType() {
		return this.jobType;
	}
	
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getJobCode() {
		return this.jobCode;
	}
	
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	public String getJobStatus() {
		return this.jobStatus;
	}
	
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getCpyCode() {
		return this.cpyCode;
	}
	
	public void setCpyCode(String cpyCode) {
		this.cpyCode = cpyCode;
	}
	public String getThirdCode() {
		return this.thirdCode;
	}
	
	public void setThirdCode(String thirdCode) {
		this.thirdCode = thirdCode;
	}

}

