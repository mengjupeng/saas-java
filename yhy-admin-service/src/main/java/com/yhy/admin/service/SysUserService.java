package com.yhy.admin.service;

import com.yhy.admin.dao.CpyDao;
import com.yhy.admin.dao.SysUserDao;
import com.yhy.admin.vo.CpyVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 上午10:08 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserService extends BaseMainService<SysUser> {

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    protected SysUserDao getDao() {
        return sysUserDao;
    }

    @Override
    protected void preInsert(SysUser entity) {
        super.preInsert(entity);
    }

    public void updateAvatar(String userAccount, String avatar) {
        sysUserDao.updateAvatar(userAccount, avatar);
    }

    public void updateCpyOrgSet(String userAccount,String cpyCode,String orgCode) {
        sysUserDao.updateCpyOrgSet(userAccount, cpyCode, orgCode);
    }

    public void updateOrgSet(String userAccount, String orgCode) {
        sysUserDao.updateOrgSet(userAccount, orgCode);
    }

}
