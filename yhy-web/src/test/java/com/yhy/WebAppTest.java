package com.yhy;
import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.service.mng.CpyMngService;
import com.yhy.admin.service.mng.FtpManageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

/**
 * <br>
 * <b>功能：</b><br>
 * <b>作者：</b>yanghuiyuan<br>
 * <b>日期：</b> 2020/3/19 <br>
 * <b>版权所有：<b>版权所有(C) 2020<br>
 */
@ActiveProfiles("dev")
@RunWith(SpringRunner.class) //14.版本之前用的是SpringJUnit4ClassRunner.class
@SpringBootTest(classes = WebMainApp.class) //1.4版本之前用的是//@SpringApplicationConfiguration(classes = Application.class)
public class WebAppTest {

    @Autowired
    private FtpManageService ftpManageService;
    @Autowired
    private CpyMngService cpyMngService;

    @Test
    public void uploadAttachment() throws Exception {
        System.out.println("daf");
        ftpManageService.begin();
        ftpManageService.putAttachment("/2020","sadfsdf.txt",new File("C:\\yanghuiyuan\\bct\\test.txt"));
        File outFile = new File("C:\\yanghuiyuan\\bct\\test_download.txt");
        ftpManageService.downloadAttachment("/2020/sadfsdf.txt",outFile);
        //ftpManageService.removeAttachment("/2020/sadfsdf.txt");
        ftpManageService.end();
    }

    @Test
    public void testExport() throws Exception {
        OutputStream outputFile = new FileOutputStream("C:\\yanghuiyuan\\bct\\test_export.xlsx");
        CpyDTO cpyDTO = new CpyDTO();
        cpyMngService.processExport(cpyDTO, outputFile);
    }

    @Test
    public void testImport() throws Exception {
        InputStream inputStream = new FileInputStream("C:\\yanghuiyuan\\saaserp\\myproject\\java\\myerp\\yhy-admin-service\\src\\main\\resources\\jxls\\import\\admin\\template\\cpy_test_template.xls");
        CpyDTO cpyDTO = new CpyDTO();
        cpyMngService.processImport(cpyDTO, inputStream);
    }



}
