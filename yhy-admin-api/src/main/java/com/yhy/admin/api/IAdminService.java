package com.yhy.admin.api;

import com.yhy.admin.dto.DataDictApiDTO;
import com.yhy.admin.dto.OrgSetApiDTO;
import com.yhy.admin.dto.RoleApiDTO;
import com.yhy.common.vo.SysAttachment;
import com.yhy.common.vo.SysUser;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-6-3 下午4:01 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public interface IAdminService {

    String SERVICE_BEAN = "apiAdminService";

    DataDictApiDTO getDataDictByTypeAndCode(String dictType, String dictCode, String lanCode);

    SysUser getUserByUserAccountAndCpyCode(String userAccount, String cpyCode);

    RoleApiDTO getRoleByRoleId(String roleId);

    List<SysUser> getUserByRoleId(String roleId);

    Set<String> getRoleFromByUserAccount(String userAccount, String cpyCode);

    Map<String,OrgSetApiDTO> getOrgsetByCode(Set<String> orgCodes);

    Boolean upload(SysAttachment sysAttachment, MultipartFile file, Boolean isUploadFtp);

}
