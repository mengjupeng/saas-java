package com.yhy.common.vo;

import com.yhy.common.jxls.MyJxlsExportConfig;
import com.yhy.common.jxls.MyJxlsImportConfig;
import com.yhy.common.utils.ToStringBean;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public class BusModuleSettingItem extends ToStringBean implements Serializable {

	/**
	 * 业务模块
	 */
	private String busModule;

	private Boolean isComplexBus = true;
	private Boolean isSimpleHistoryMng = false;
	private Boolean isOrgsetMng = false;
	private Boolean isAttachmentMng = false;
	private Boolean isNeedBarcode = false;
	private Boolean isNeedFeedback = false;
	private String busCodePrefix;
	private String idPrefix;
	private String busMngPrefix;
	private String mainTable;
	private String busExtendTable;
	private String busCommonStateTable;
	private String busCommonStateHistoryTable;

	private String sysAttachmentTable;
	private MyJxlsExportConfig jxlsExportConfig;
	private MyJxlsImportConfig jxlsImportConfig;

	public MyJxlsImportConfig getJxlsImportConfig() {
		return jxlsImportConfig;
	}

	public void setJxlsImportConfig(MyJxlsImportConfig jxlsImportConfig) {
		this.jxlsImportConfig = jxlsImportConfig;
	}

	public MyJxlsExportConfig getJxlsExportConfig() {
		return jxlsExportConfig;
	}

	public void setJxlsExportConfig(MyJxlsExportConfig jxlsExportConfig) {
		this.jxlsExportConfig = jxlsExportConfig;
	}
	public Boolean isNeedBarcode() {
		return isNeedBarcode;
	}

	public void setNeedBarcode(Boolean needBarcode) {
		isNeedBarcode = needBarcode;
	}
	public Boolean isAttachmentMng() {
		return isAttachmentMng;
	}

	public void setAttachmentMng(Boolean attachmentMng) {
		isAttachmentMng = attachmentMng;
	}

	public Boolean isOrgsetMng() {
		return isOrgsetMng;
	}

	public void setOrgsetMng(Boolean orgsetMng) {
		isOrgsetMng = orgsetMng;
	}

	public String getBusMngPrefix() {
		return busMngPrefix;
	}

	public void setBusMngPrefix(String busMngPrefix) {
		this.busMngPrefix = busMngPrefix;
	}

	public String getBusModule() {
		return busModule;
	}

	public void setBusModule(String busModule) {
		this.busModule = busModule;
	}

	public Boolean isSimpleHistoryMng() {
		return isSimpleHistoryMng;
	}

	public void setSimpleHistoryMng(Boolean simpleHistoryMng) {
		isSimpleHistoryMng = simpleHistoryMng;
	}

	public boolean isComplexBus() {
		return isComplexBus;
	}

	public void setComplexBus(boolean complexBus) {
		isComplexBus = complexBus;
	}

	public String getBusCodePrefix() {
		return busCodePrefix;
	}

	public String getIdPrefix() {
		return idPrefix;
	}

	public void setIdPrefix(String idPrefix) {
		this.idPrefix = idPrefix;
	}

	public void setBusCodePrefix(String busCodePrefix) {
		this.busCodePrefix = busCodePrefix;
	}

	public String getMainTable() {
		return StringUtils.defaultIfBlank(this.mainTable, "");
	}

	public void setMainTable(String mainTable) {
		this.mainTable = mainTable;
	}

	public String getSysAttachmentTable() {
		return StringUtils.defaultIfBlank(this.sysAttachmentTable, "T_SYS_ATTACHMENT");
	}

	public void setSysAttachmentTable(String sysAttachmentTable) {
		this.sysAttachmentTable = sysAttachmentTable;
	}

	public String getBusCommonStateTable() {
		return StringUtils.defaultIfBlank(this.busCommonStateTable, "T_BUS_COMMON_STATE");
	}

	public String getBusExtendTable() {
		return StringUtils.defaultIfBlank(this.busExtendTable, "T_BUS_EXTEND");
	}

	public void setBusExtendTable(String busExtendTable) {
		this.busExtendTable = busExtendTable;
	}

	public String getBusCommonStateHistoryTable() {
		return StringUtils.defaultIfBlank(this.busCommonStateHistoryTable, "TH_BUS_COMMON_STATE");
	}

	public void setBusCommonStateHistoryTable(String busCommonStateHistoryTable) {
		this.busCommonStateHistoryTable = busCommonStateHistoryTable;
	}

	public void setBusCommonStateTable(String busCommonStateTable) {
		this.busCommonStateTable = busCommonStateTable;
	}

	public Boolean isNeedFeedback() {
		return isNeedFeedback;
	}

	public void setNeedFeedback(Boolean needFeedback) {
		isNeedFeedback = needFeedback;
	}


}
