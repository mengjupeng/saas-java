package com.yhy.common.dao;

import com.yhy.common.vo.BusCommonState;
import com.yhy.common.vo.SysAttachment;
import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-9-2 下午3:28 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysAttachmentDao")
public interface SysAttachmentDao extends BaseDao<SysAttachment> {

    SysAttachment findByBusModuleAndId(@Param("busModule") String busModule,@Param("id") String id);

    int deleteByMainId(@Param("busModule") String busModule, @Param("mainId") String mainId);

    List<SysAttachment> findByMainId(@Param("busModule") String busModule, @Param("mainId") String mainId);

}
