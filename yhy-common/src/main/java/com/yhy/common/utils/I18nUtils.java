package com.yhy.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * <br>
 * <b>功能：</b>资源信息工具类<br>
 * <b>作者：</b>yanghuiyuan<br>
 * <b>日期：</b> 2019-9-23 <br>
 * <b>版权所有：<b>版权所有(C) 2019<br>
 */
public class I18nUtils {

	/**
	 * 获取单个国际化翻译值
	 */
	public static String get(String code) {
		return get(code,null,null);
	}

	public static String get(String code, Object[] params, String defaultMessage) {
		return SpringContextHolder.getApplicationContext().getMessage(code, params,  defaultMessage, LocaleContextHolder.getLocale());
	}

}
