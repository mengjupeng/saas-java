package com.yhy.common.utils;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * <br>
 * <b>功能：分段事务执行
 */
@Component
public class TransationExecuteContext {

	@Transactional
	public <T, E> T execute(ITransationExecutor<T, E> transationExecutor, E param) {
		return transationExecutor.execute(param);
	}

	public static TransationExecuteContext getTransationExecuteContext() {
		return SpringContextHolder.getBean(TransationExecuteContext.class);
	}
}
