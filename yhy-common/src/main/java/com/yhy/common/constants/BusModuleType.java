package com.yhy.common.constants;

import com.yhy.common.exception.CustomRuntimeException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public enum BusModuleType {

	/**
	 * 组织机构
	 */
	MD_ORG_SET_MNG("100001"),

	//公司信息管理
	MD_CPY_MNG("100002"),

	/**
	 * 用户管理
	 */
	MD_USER_MNG("100003"),

	/**
	 * 数据字典
	 */
	MD_DATA_DICT_MNG("100004"),

	/**
	 * 菜单管理
	 */
	MD_MENU_MNG("100005"),
	/**
	 * 系统权限定义管理
	 */
	MD_SYS_PRIV_MNG("100006"),

	/**
	 * 系统角色定义管理
	 */
	MD_SYS_ROLE_MNG("100007"),

	/**
	 * redis缓存管理
	 */
	MD_REDIS_MNG("100008"),

	/**
	 * redis缓存管理
	 */
	MD_QUARTZ_JOB_MNG("100009"),

	/*
	工作流定义管理
	 */
	MD_WORKFLOW_DEF_MNG("100010"),

	/**
	 工作流配置管理
	 */
	MD_WORKFLOW_CONFIG_MNG("100011"),

	/**
	 动态表单管理
	 */
	MD_DYNAMIC_FORM_MNG("100012"),

	/*
	* 表单申请管理
	 */
	MD_FORM_APPLY_MNG("100015"),

	/**
	 ftp配置管理
	 */
	MD_FTP_CONFIG_MNG("100016"),

	/**
	 职位管理
	 */
	MD_SYS_JOB_MNG("100013"),

	/**
	 自定义报表打印管理
	 */
	MD_SYS_PRINT_MNG("100014");


	private final String val;

	private static Map<String, BusModuleType> moduleTypeMap;

	BusModuleType(String val) {
		this.val = val;
	}

	public String getVal() {
		return val;
	}

	/**
	 * 根据操作类型的值获取业务类型
	 * @param val
	 * @return
	 */
	public static BusModuleType getInstByVal(String val) {
		if(moduleTypeMap == null) {
			synchronized (BusModuleType.class) {
				if (moduleTypeMap == null) {
					moduleTypeMap = new HashMap<String, BusModuleType>();
					for (BusModuleType moduleType : BusModuleType.values()) {
						moduleTypeMap.put(moduleType.getVal(), moduleType);
					}
				}
			}
		}

		if (!moduleTypeMap.containsKey(val)) {
			throw new CustomRuntimeException("ModuleType模块值" + val + "对应枚举值不存在。");
		}
		return moduleTypeMap.get(val);
	}

}
