package com.yhy.common.constants;

import com.yhy.common.exception.CustomRuntimeException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-26 上午11:25 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public enum BusMenuType {

	/**
	 * 外部菜单
	 */
	MENU_EXTERNAL("1"),

	//内部菜单
	MENU_INNER("2"),

	//权限标识
	MENU_PRIV("3");

	private final String val;

	private static Map<String, BusMenuType> typeMap;

	BusMenuType(String val) {
		this.val = val;
	}

	public String getVal() {
		return val;
	}

	/**
	 * 根据操作类型的值获取业务类型
	 * @param val
	 * @return
	 */
	public static BusMenuType getInstByVal(String val) {
		if(typeMap == null) {
			synchronized (BusMenuType.class) {
				if (typeMap == null) {
					typeMap = new HashMap<String, BusMenuType>();
					for (BusMenuType moduleType : BusMenuType.values()) {
						typeMap.put(moduleType.getVal(), moduleType);
					}
				}
			}
		}

		if (!typeMap.containsKey(val)) {
			throw new CustomRuntimeException("MenuType模块值" + val + "对应枚举值不存在。");
		}
		return typeMap.get(val);
	}

}
