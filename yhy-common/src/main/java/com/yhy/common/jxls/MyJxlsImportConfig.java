package com.yhy.common.jxls;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午8:21 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class MyJxlsImportConfig {

	/*
	* 模板路径
	 */
	private String templatePath;
	/**
	 * 模版名稱
	 */
	private String templateName;
	/**
	 * reader 配置文件路徑
	 */
	private String configPath;

	/**
	 * 處理類
	 */
	private String importSupportBean;


	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getConfigPath() {
		return configPath;
	}

	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	public String getImportSupportBean() {
		return importSupportBean;
	}

	public void setImportSupportBean(String importSupportBean) {
		this.importSupportBean = importSupportBean;
	}
}
