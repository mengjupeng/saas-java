package com.yhy.common.config;

import com.yhy.common.utils.SpringContextHolder;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.core.env.Environment;

@SpringBootConfiguration
public class EnvConfig {

    //@Autowired//通过注解直接注入对象
    private static Environment environment;

   // @Value("${invoke.inner.url}")
    //private String hostUrl;


    public void set(){

    }

    public static String getValueByPropertyName(String propertyName) {
        return getValueByPropertyName(propertyName,"");
    }

    public static String getValueByPropertyName(String propertyName, String defaultValue) {
        if(environment == null) {
            synchronized(EnvConfig.class) {
                if(environment == null) {
                    environment = SpringContextHolder.getBean(Environment.class);
                }
            }
        }
        return environment.getProperty(propertyName,defaultValue);
    }

}
